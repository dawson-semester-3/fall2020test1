package utilities;

public class MatrixMethod {
	public static int[][] duplicate(int[][] oldArr) {
		int[][] newArr = new int[oldArr.length][oldArr[1].length * 2];
		for (int i = 0; i < newArr.length; i++) {
			for (int j = 0; j < newArr[i].length; j++) {
				newArr[i][j] = oldArr[i][j / 2];
			}
		}
		return newArr;
	}
}