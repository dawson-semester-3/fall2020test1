package tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import utilities.MatrixMethod;

class MatrixMethodTests {
	@Test
	void duplicateTest() {
		// using Arrays.deepToString() to make things easier
		assertEquals(Arrays.deepToString(new int[][] { { 1, 1, 2, 2, 3, 3 }, { 4, 4, 5, 5, 6, 6 } }),
				Arrays.deepToString(MatrixMethod.duplicate(new int[][] { { 1, 2, 3 }, { 4, 5, 6 } })));
	}

}
