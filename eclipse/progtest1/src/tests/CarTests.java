package tests;

import automobiles.Car;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CarTests {
	Car car = new Car(60);

	@Test
	void getSpeedTest() {
		assertEquals(60, car.getSpeed());
	}
	
	@Test
	void getLocationTest() {
		assertEquals(50, car.getLocation());
	}
	
	@Test
	void moveRightTest() {
		car.moveRight();
		assertEquals(100, car.getLocation());
	}

	@Test
	void moveLeftTest() {
		car.moveLeft();
		assertEquals(0, car.getLocation());
	}
	
	@Test
	void accelerateTest() {
		car.accelerate();
		assertEquals(61, car.getSpeed());
	}
	
	@Test
	void stopTest() {
		car.stop();
		assertEquals(0, car.getSpeed());
	}
}
